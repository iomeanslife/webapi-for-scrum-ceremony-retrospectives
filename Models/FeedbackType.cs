namespace ScrumCeremonies.Models
{
    public enum FeedbackType
    {
        Positive,
        Negative,
        Idea,
        Praise
    }
}