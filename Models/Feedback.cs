namespace ScrumCeremonies.Models
{
    public class Feedback
    {
        public string Name { get; set; }
        public string Body { get; set; }
        public FeedbackType Type { get; set; }
    }
}