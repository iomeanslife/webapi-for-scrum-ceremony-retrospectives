using System;
using System.Collections;
using System.Collections.Generic;
namespace ScrumCeremonies.Models
{
    public class Ceremony
    {
        // Identifier
        public string Name { get; set; }
        public string Summary { get; set; }
        public DateTime Date { get; set; }
        public List<String> Participants { get; set; }
        public List<Feedback> FeedbackItems { get; set; }
    }
}