# Technical .Net Test - WebAPI for Scrum Ceremony Retrospectives

## Used tools, SDK, frameworks
+ Visual Studio Code
+ Git
+ Github
+ https://www.gitignore.io/
+ .Net Core
+ asp.Net Core
+ Postman

## Usage
__Note:__ This code uses memory caching so inserted data will be when restarting.

+ Retrieving all ceremonies: https://localhost:5001/Ceremonies.
+ Retrieving ceremonies by date: https://localhost:5001/ByDate/2020-01-21.
+ Creating a new ceremony: https://localhost:5001/Ceremonies as `POST` with a JSON body of a ceremony.
+ Adding feedback items to a ceremony: https://localhost:5001/Ceremonies/Feedback?name=Omar with a JSON body a feedback item.

## Process
+ Starting out with the .net core webapi template.
+ Adding assets for vs code to enable building and debugging.
+ Remove exaample controller.
+ Using https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-web-api?view=aspnetcore-3.1&tabs=visual-studio-code for reference.
+ Not using asynchronous code due to not needing it.
+ adding xml serialization (WIP),

## Missing
+ Unit Testing
+ Frontend

## Sample Data
__NOTE:__ Data can't be inserted as is, feedback items needed to be added individually after creating the Ceremony Retrospective.

``` JSON
[
    {
        "name": "Retrospective 1",
        "summary": "“Post release retrospective",
        "date": "2016-07-27",
        "participants": [
            "Victor","Gareth","Mike"            
        ],
        "feedbackItems": [
            {
                "name": "Gareth",
                "body": "Sprint objective met",
                "type": 0
            },
            {
                "name": "Viktor",
                "body": "Too many items piled up in the awaiting QA",
                "type": 1
            },
            {
                "name": "Mike",
                "body": "We should be looking to start using VS2015",
                "type": 2
            }
        ]
    }
]
```