﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using ScrumCeremonies.Models;

namespace ScrumCeremonies.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CeremoniesController : ControllerBase
    {
        private readonly IMemoryCache _memoryCache;
        private readonly ILogger<CeremoniesController> _logger;

        public CeremoniesController(ILogger<CeremoniesController> logger, IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
            _logger = logger;
        }

        [HttpGet()]
        public IEnumerable<Ceremony> Get()
        {
            try
            {
                var collection = _memoryCache.Get<Dictionary<string, Ceremony>>("CeremonyCollection");
                return collection?.Values;
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, "Failure: Retrieving ceremonies failed.");
                throw;
            }
        }

        [HttpGet()]
        [Route("ByDate/{date:datetime}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetByDate(DateTime date)
        {
            if (date == default(DateTime))
            {
                _logger.LogError("Failure: Invalid date.");
                return BadRequest();
            }

            try
            {
                var collection = _memoryCache.Get<Dictionary<string, Ceremony>>("CeremonyCollection");
                return Ok(collection?.Values.Where(x => x.Date.Date == date.Date));
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, "Failure: Retrieving ceremonies failed.");
                throw;
            }
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Post(Ceremony ceremony)
        {
            if (ceremony.FeedbackItems?.Count() > 0)
            {
                _logger.LogError("Failure: Pre existing feedback items.");
                return BadRequest();
            }

            if (ceremony.Date == default(DateTime) || ceremony.Participants.Count() < 1)
            {
                _logger.LogError("Failure: No date or name specified.");
                return BadRequest();
            }

            try
            {
                var collection = _memoryCache.Get<Dictionary<string, Ceremony>>("CeremonyCollection") ?? new Dictionary<string, Ceremony>();
                ceremony.FeedbackItems = new List<Feedback>();
                collection.Add(ceremony.Name, ceremony);
                _memoryCache.Set("CeremonyCollection", collection);

                return Ok();
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, "Failure: Storign the ceremony was unsuccesful.");
                throw;
            }
        }

        [HttpPost("Feedback")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult PostFeedback([FromBody]Feedback feedback, string name)
        {
            try
            {
                var collection = _memoryCache.Get<Dictionary<string, Ceremony>>("CeremonyCollection") ?? new Dictionary<string, Ceremony>();
                collection[name]?.FeedbackItems.Add(feedback);

                _memoryCache.Set("CeremonyCollection", collection);

                return Ok();
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, "Failure: Storign the ceremony was unsuccesful.");
                throw;
            }
        }
    }
}
